package it.unkey.e_commerce.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class RegisterDTO {

    @NotBlank
    private String name;

    @NotBlank
    @Length(min = 4, max = 15, message = "the password must be between 4 and 15 letters")
    private String password;

    @Email
    private String email;
}
