package it.unkey.e_commerce.DTO;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class ProductDTO {

    @Min( value = 1)
    private int id;

    @NotBlank
    private  String name;

    @NotNull
    private CategoryDTO category;

    @NotBlank
    private String details;

    @Min(value = 1)
    private double price;

}
