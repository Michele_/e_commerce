package it.unkey.e_commerce.DTO;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleUserDTO {

    @Min(value = 1)
    private int id;

    @NotBlank
    private String roleName;

    @NotNull
    Set<UserDTO> users;

}
