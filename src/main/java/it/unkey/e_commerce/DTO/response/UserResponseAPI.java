package it.unkey.e_commerce.DTO.response;

import it.unkey.e_commerce.DTO.UserDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserResponseAPI {

    private UserDTO userDTO;

    private String error;

    public UserResponseAPI(UserDTO userDTO) {
        this.userDTO = userDTO;
    }

    public UserResponseAPI(String error) {
        this.error = error;
    }
}
