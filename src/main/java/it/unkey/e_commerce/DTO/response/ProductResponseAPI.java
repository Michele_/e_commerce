package it.unkey.e_commerce.DTO.response;

import it.unkey.e_commerce.DTO.ProductDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ProductResponseAPI {

    private ProductDTO productDTO;

    private String error;

    public ProductResponseAPI(ProductDTO productDTO) {
        this.productDTO = productDTO;
    }

    public ProductResponseAPI(String error) {
        this.error = error;
    }
}
