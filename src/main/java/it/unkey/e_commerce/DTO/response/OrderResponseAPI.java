package it.unkey.e_commerce.DTO.response;

import it.unkey.e_commerce.DTO.CategoryDTO;
import it.unkey.e_commerce.DTO.OrderDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OrderResponseAPI {

    private OrderDTO orderDTO;

    private String error;

    public OrderResponseAPI(OrderDTO orderDTO) {
        this.orderDTO = orderDTO;
    }

    public OrderResponseAPI(String error) {
        this.error = error;
    }
}
