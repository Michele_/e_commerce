package it.unkey.e_commerce.DTO.response;

import it.unkey.e_commerce.DTO.CategoryDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CategoryResponseAPI {

    private CategoryDTO categoryDTO;

    private String error;

    public CategoryResponseAPI(CategoryDTO categoryDTO) {
        this.categoryDTO = categoryDTO;
    }

    public CategoryResponseAPI(String error) {
        this.error = error;
    }
}
