package it.unkey.e_commerce.DTO;

import it.unkey.e_commerce.entity.UserEntity;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.util.Set;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class OrderDTO {

    @Min(value = 1)
    private int id;

    @NotNull
    private Set<ProductDTO> products;

    @Min(value = 1)
    private double totalPrice;

    private UserDTO user;

    private boolean isSold;

}
