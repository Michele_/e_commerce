package it.unkey.e_commerce.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "role", schema = "e_commerce")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class RoleUserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "role_id", nullable = false)
    private int id;

    @Basic
    @Column(name = "role_name", nullable = false)
    private String name;

    @OneToMany(mappedBy = "role", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
    private Set<UserEntity> users;

    public RoleUserEntity(int id) {
        this.id = id;
        this.users = new HashSet<>();
    }

}
