package it.unkey.e_commerce.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "product", schema = "e_commerce")
@Getter
@Setter
public class ProductEntity {

    @Id
    @Column(name = "product_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Basic
    @Column(name = "product_name", nullable = false, length = 40)
    private String name;

    @Basic
    @Column(name = "product_price", nullable = false)
    private double price;

    @Basic
    @Column(name = "product_details", nullable = false)
    private String details;

    @ManyToOne
    @JoinColumn(name = "category_id", nullable = false)
    private CategoryEntity category;


    @ManyToMany(mappedBy = "products", fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
    private Set<OrderEntity> orders;

    public ProductEntity() {
         this.orders = new HashSet<>();
    }
}
