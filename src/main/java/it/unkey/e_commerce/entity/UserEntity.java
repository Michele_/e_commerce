package it.unkey.e_commerce.entity;

import lombok.Getter;
import lombok.Setter;

import javax.management.relation.Role;
import javax.persistence.*;
import java.util.Set;

@Entity
@Getter
@Setter
@Table(name = "user", schema = "e_commerce")
public class UserEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "user_id", nullable = false)
    private int id;

    @Basic
    @Column(name = "user_name", nullable = false, length = 50)
    private String name;

    @Basic
    @Column(name = "user_password", nullable = false, length = 60)
    private String password;

    @Basic
    @Column(name = "user_email", nullable = false, length = 80)
    private String email;

    @ManyToOne
    @JoinColumn(name = "role_id", nullable = false)
    private RoleUserEntity role;

    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY,cascade = CascadeType.MERGE)
    private Set<OrderEntity> orders;

    public UserEntity() {
        this.role = new RoleUserEntity(2);
    }
}
