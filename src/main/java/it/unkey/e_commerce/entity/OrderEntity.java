package it.unkey.e_commerce.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "order_store", schema = "e_commerce")
@Getter
@Setter
@AllArgsConstructor

public class OrderEntity {

    @Id
    @Column(name = "order_id", nullable = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Basic
    @Column(name = "order_total_price")
    private double totalPrice;

    @ManyToOne
    @JoinColumn(name="user_id",nullable = false)
    private UserEntity user;

    @Column(name = "is_sold")
    private boolean isSold;

    /*@ManyToMany(mappedBy = "orders",fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})*/
    @ManyToMany(fetch = FetchType.LAZY, cascade = {CascadeType.MERGE})
    @JoinTable(name = "order_product",
            joinColumns = {@JoinColumn(name = "order_id")},
            inverseJoinColumns = {@JoinColumn(name = "product_id")})
    private Set<ProductEntity> products;

    public OrderEntity() {
        this.products = new HashSet<>();
    }
}
