package it.unkey.e_commerce.mappers;

import it.unkey.e_commerce.DTO.OrderDTO;
import it.unkey.e_commerce.entity.OrderEntity;
import org.mapstruct.Mapper;

import java.util.Set;

@Mapper(componentModel = "spring")
public interface OrderMapper {
    OrderEntity fromOrderDTOToEntity(OrderDTO orderDTO);
    OrderDTO fromOrderEntityToDTO(OrderEntity orderEntity);
    Set<OrderDTO> fromOrderEntitySetToOrderDTOSet(Set<OrderEntity> orderEntities);
}
