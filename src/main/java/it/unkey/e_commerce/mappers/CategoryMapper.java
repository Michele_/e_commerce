package it.unkey.e_commerce.mappers;

import it.unkey.e_commerce.DTO.CategoryDTO;
import it.unkey.e_commerce.entity.CategoryEntity;
import org.mapstruct.Mapper;
import java.util.Set;

@Mapper(componentModel = "spring")
public interface CategoryMapper {
    CategoryEntity fromCategoryDTOToCategoryEntity(CategoryDTO categoryDTO);
    CategoryDTO fromCategoryEntityToCategoryDTO(CategoryEntity categoryEntity);
    Set<CategoryDTO> fromCategoryEntitySetToCategoryDTOSet(Set<CategoryEntity> categoryEntities);
}
