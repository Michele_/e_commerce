package it.unkey.e_commerce.mappers;

import it.unkey.e_commerce.DTO.RegisterDTO;
import it.unkey.e_commerce.DTO.UserDTO;
import it.unkey.e_commerce.entity.UserEntity;
import org.mapstruct.Mapper;

import java.util.Set;

@Mapper(componentModel = "spring")
public interface UserMapper {

    UserEntity fromUserDTOToUserEntity(UserDTO userDTO);
    UserDTO fromUserEntityToUserDTO(UserEntity userEntity);
    UserEntity fromRegisterDTOToUserEntity(RegisterDTO registerDTO);
    Set<UserDTO> fromEntitySetToDTOSet(Set<UserEntity> users);
}
