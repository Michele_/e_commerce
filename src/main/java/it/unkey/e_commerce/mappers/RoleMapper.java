package it.unkey.e_commerce.mappers;

import it.unkey.e_commerce.DTO.RoleUserDTO;
import it.unkey.e_commerce.entity.RoleUserEntity;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface RoleMapper {

    RoleUserEntity fromRoleUserDTOToRoleUserEntity(RoleUserDTO roleUserDTO);
    RoleUserDTO fromRoleUserEntityToRoleUserDTO(RoleUserEntity roleUserEntity);


}
