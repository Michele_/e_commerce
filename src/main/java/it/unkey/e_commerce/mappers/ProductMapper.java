package it.unkey.e_commerce.mappers;

import it.unkey.e_commerce.DTO.ProductDTO;
import it.unkey.e_commerce.entity.ProductEntity;
import org.mapstruct.Mapper;

import java.util.Set;
@Mapper(componentModel = "spring")
public interface ProductMapper {
    ProductEntity fromProductDTOToProductEntity(ProductDTO productDTO);
    ProductDTO fromProductEntityToProductDTO(ProductEntity productEntity);
    Set<ProductDTO> fromProductEntitySetToProductDTOSet(Set<ProductEntity> products);
}
