package it.unkey.e_commerce.services;

import it.unkey.e_commerce.DTO.OrderDTO;
import it.unkey.e_commerce.DTO.ProductDTO;
import it.unkey.e_commerce.entity.OrderEntity;
import it.unkey.e_commerce.entity.ProductEntity;
import it.unkey.e_commerce.exceptions.IdNotFoundException;
import it.unkey.e_commerce.exceptions.ItemNotInsertedException;
import it.unkey.e_commerce.exceptions.ItemNotUpdatedException;
import it.unkey.e_commerce.interfaces.OrderService;
import it.unkey.e_commerce.mappers.OrderMapper;
import it.unkey.e_commerce.mappers.ProductMapper;
import it.unkey.e_commerce.repositories.OrderRepository;
import it.unkey.e_commerce.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

@Service
public class OrderServiceImpl implements OrderService<OrderDTO> {

    private OrderRepository orderRepository;
    private OrderMapper orderMapper;
    private ProductRepository productRepository;
    private ProductMapper productMapper;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository, OrderMapper orderMapper, ProductRepository productRepository, ProductMapper productMapper) {
        this.orderRepository = orderRepository;
        this.orderMapper = orderMapper;
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    @Override
    public OrderDTO getById(int id) {
        return orderMapper.fromOrderEntityToDTO(orderRepository.findById(id)
                .orElseThrow( () -> new IdNotFoundException("Order id  not found")));
    }

    @Override
    public Set<OrderDTO> getAll() {
        return orderMapper.fromOrderEntitySetToOrderDTOSet(orderRepository.findAll().stream().collect(Collectors.toSet()));
    }

    @Override
    public OrderDTO update(OrderDTO orderDTO) {
        if (isOrderPresent(orderDTO.getId())) {
            OrderEntity order = orderMapper.fromOrderDTOToEntity(orderDTO);
            return orderMapper.fromOrderEntityToDTO(orderRepository.save(order));
        } else
            throw new ItemNotUpdatedException("could not update order");
    }

    @Override
    public void delete(int id) {
        if (id > 0)
            orderRepository.deleteById(id);
        else throw  new IdNotFoundException("could not delete order with this Id");
    }

    @Override
    public OrderDTO insert(OrderDTO orderDTO) {
        OrderEntity order = orderMapper.fromOrderDTOToEntity(orderDTO);
        Set<ProductEntity> products = new HashSet<>();
        double price = 0;
        for(ProductDTO productDTO : orderDTO.getProducts()) {
            price+= productDTO.getPrice();
            products.add(productMapper.fromProductDTOToProductEntity(productDTO));
        }
        order.setTotalPrice(price);
        if (order.getId() == 0)
            return orderMapper.fromOrderEntityToDTO(orderRepository.save(order));
        throw new ItemNotInsertedException("order not inserted");
    }

    @Override
    public OrderDTO patchUpdate(OrderDTO orderDTO) {
        OrderEntity orderEntity = orderRepository.findById(orderDTO.getId()).orElseThrow(()-> new ItemNotUpdatedException("order not updated"));
        return orderMapper.fromOrderEntityToDTO(orderRepository.save(orderEntity));
    }
    
    @Override
    public OrderDTO deleteProduct(OrderDTO orderDTO, int id) {
        OrderEntity orderEntity = orderMapper.fromOrderDTOToEntity(orderDTO);
        orderEntity.getProducts().stream().forEach(product -> productRepository.deleteById(id));
        return orderMapper.fromOrderEntityToDTO(orderEntity);
    }

    private boolean isOrderPresent(int id) {
        return orderRepository.findById(id).isPresent();
    }
}
