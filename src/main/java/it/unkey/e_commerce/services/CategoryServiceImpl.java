package it.unkey.e_commerce.services;

import it.unkey.e_commerce.DTO.CategoryDTO;
import it.unkey.e_commerce.entity.CategoryEntity;
import it.unkey.e_commerce.exceptions.ItemNotFoundException;
import it.unkey.e_commerce.interfaces.CategoryService;
import it.unkey.e_commerce.mappers.CategoryMapper;
import it.unkey.e_commerce.repositories.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.Set;

@Service
@Qualifier("categoryService")
public class CategoryServiceImpl implements CategoryService<CategoryDTO> {

    private CategoryMapper categoryMapper;
    private CategoryRepository categoryRepository;

    @Autowired
    public CategoryServiceImpl(CategoryMapper categoryMapper, CategoryRepository categoryRepository) {
        this.categoryMapper = categoryMapper;
        this.categoryRepository = categoryRepository;
    }

    @Override
    public Set<CategoryDTO> getAll() {
        Set<CategoryEntity> categoryEntities = new LinkedHashSet<>();
        categoryRepository.findAll().forEach(categoryEntities::add);
        return categoryMapper.fromCategoryEntitySetToCategoryDTOSet(categoryEntities);

    }

    @Override
    public CategoryDTO getById(int id) {
        return categoryMapper.fromCategoryEntityToCategoryDTO(categoryRepository.findById(id).orElseThrow(() -> new ItemNotFoundException("category not found")));

    }

    @Override
    public CategoryDTO getByName(String name) {
        return categoryMapper.fromCategoryEntityToCategoryDTO(categoryRepository.findByName(name).orElseThrow(() -> new ItemNotFoundException("category not found")));

    }
}
