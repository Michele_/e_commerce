package it.unkey.e_commerce.services;

import it.unkey.e_commerce.DTO.ProductDTO;
import it.unkey.e_commerce.entity.ProductEntity;
import it.unkey.e_commerce.exceptions.IdNotFoundException;
import it.unkey.e_commerce.exceptions.ItemNotFoundException;
import it.unkey.e_commerce.exceptions.ItemNotUpdatedException;
import it.unkey.e_commerce.interfaces.ProductService;
import it.unkey.e_commerce.mappers.CategoryMapper;
import it.unkey.e_commerce.mappers.ProductMapper;
import it.unkey.e_commerce.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import java.util.LinkedHashSet;
import java.util.Set;

@Service
@Qualifier("productService")
public class ProductServiceImpl implements ProductService<ProductDTO> {
    private ProductRepository productRepository;
    private ProductMapper productMapper;
    private CategoryMapper categoryMapper;

    @Autowired
    public ProductServiceImpl(ProductRepository productRepository, ProductMapper productMapper, CategoryMapper categoryMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
        this.categoryMapper = categoryMapper;
    }

    @Override
    public ProductDTO insert(ProductDTO productDTO) {
        ProductEntity productEntity = productMapper.fromProductDTOToProductEntity(productDTO);
        return productMapper.fromProductEntityToProductDTO(productRepository.save(productEntity));
    }

    @Override
    public ProductDTO patchUpdate(ProductDTO productDTO) {
        ProductEntity productEntity = productRepository.findById(productDTO.getId()).orElseThrow(() -> new IdNotFoundException("id not found"));

        if (productDTO.getCategory() != null)
            productEntity.setCategory(categoryMapper.fromCategoryDTOToCategoryEntity(productDTO.getCategory()));

        if (productDTO.getPrice() != 0)
            productEntity.setPrice(productDTO.getPrice());

        if (productDTO.getDetails() != null)
            productEntity.setDetails(productDTO.getDetails());

        if (productDTO.getName() != null)
            productEntity.setName(productDTO.getName());

        return productMapper.fromProductEntityToProductDTO(productRepository.save(productEntity));
    }

    @Override
    public ProductDTO getByName(String name) {
        return productMapper.fromProductEntityToProductDTO(productRepository.getByName(name).orElseThrow(() -> new ItemNotFoundException("category not found")));
    }

    @Override
    public ProductDTO getById(int id) {
        return productMapper.fromProductEntityToProductDTO(productRepository.findById(id).orElseThrow(()-> new IdNotFoundException("id not found")));
    }

    @Override
    public Set<ProductDTO> getAll() {
        Set<ProductEntity> products = new LinkedHashSet<>();
        productRepository.findAll().forEach(products::add);
        return productMapper.fromProductEntitySetToProductDTOSet(products);
    }

    @Override
    public ProductDTO update(ProductDTO productDTO) {
        ProductEntity product = productMapper.fromProductDTOToProductEntity(productDTO);
        String name = product.getName();
        double price = product.getPrice();
        if((name != null && price != 0)) {
            productRepository.save(product);
            return productMapper.fromProductEntityToProductDTO(product);
        }else
            throw new ItemNotUpdatedException("could not update");
    }

    @Override
    public void delete(int id) {
        if(getById(id)!= null)
            productRepository.deleteById(id);
        else
            throw new IdNotFoundException("could not delete , check your id ");

    }


}
