package it.unkey.e_commerce.services;

import it.unkey.e_commerce.DTO.LoginDTO;
import it.unkey.e_commerce.DTO.RegisterDTO;
import it.unkey.e_commerce.DTO.UserDTO;
import it.unkey.e_commerce.config.PasswordEncoder;
import it.unkey.e_commerce.entity.UserEntity;
import it.unkey.e_commerce.exceptions.IdNotFoundException;
import it.unkey.e_commerce.exceptions.ItemNotUpdatedException;
import it.unkey.e_commerce.interfaces.UserService;
import it.unkey.e_commerce.mappers.UserMapper;
import it.unkey.e_commerce.repositories.ApplicationUserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

@Service
@Qualifier("userService")
public class UserDetailsServiceImpl implements UserDetailsService, UserService<UserDTO, LoginDTO, RegisterDTO> {

    private ApplicationUserRepository applicationUserRepository;
    private UserMapper userMapper;
    private PasswordEncoder passwordEncoder;


    @Autowired
    public UserDetailsServiceImpl(ApplicationUserRepository applicationUserRepository,
                                  UserMapper userMapper,
                                  PasswordEncoder passwordEncoder) {
        this.applicationUserRepository = applicationUserRepository;
        this.userMapper = userMapper;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public UserDetails loadUserByUsername(String name) throws UsernameNotFoundException {
        UserEntity applicationUser = applicationUserRepository.findByName(name);
        if (applicationUser == null) {
            throw new UsernameNotFoundException(name);
        }
        List<GrantedAuthority>  authorities =  new ArrayList<>();
        GrantedAuthority grantedAuthority = new SimpleGrantedAuthority("ROLE_" + applicationUser.getRole().getName().toUpperCase());
        authorities.add(grantedAuthority);
        return new User(applicationUser.getName(), applicationUser.getPassword(), authorities);
    }

    @Override
    public UserDTO getByUsernameAndPassword(LoginDTO login) {
        UserEntity userEntity = applicationUserRepository.findByNameAndPassword(login.getName(), login.getPassword());
        return userMapper.fromUserEntityToUserDTO(userEntity);
    }

    @Override
    public UserDTO getById(int id) {
        return userMapper.fromUserEntityToUserDTO(applicationUserRepository.findById(id).orElseThrow(()-> new IdNotFoundException("requested id does not exist")));
    }

    @Override
    public Set<UserDTO> getAll() {
        Set<UserEntity> userEntities=new LinkedHashSet<>();
        applicationUserRepository.findAll().forEach(userEntities::add);
        return userMapper.fromEntitySetToDTOSet(userEntities);
    }

    @Override
    public UserDTO update(UserDTO userDTO) {
        UserEntity user = userMapper.fromUserDTOToUserEntity(userDTO);
        String username= user.getName();
        String password=user.getPassword();
        if((username ==null && password==null)){
            applicationUserRepository.save(user);
            return userMapper.fromUserEntityToUserDTO(user);
        }else
            throw new ItemNotUpdatedException("could not update");
    }

    @Override
    public void delete(int id) {
        if (getById(id)!=null)
            applicationUserRepository.deleteById(id);
        else
            throw new IdNotFoundException("could not delete; check your id!");
    }

    @Override
    public UserDTO registerUser(RegisterDTO registerDTO){
        if(registerDTO.getName() != null){
            UserEntity userEntity = userMapper.fromRegisterDTOToUserEntity(registerDTO);
            userEntity.setName(registerDTO.getName());
            userEntity.setEmail(registerDTO.getEmail());
            userEntity.setPassword(passwordEncoder.getPasswordEncoder().encode(registerDTO.getPassword()));
            applicationUserRepository.save(userEntity);
            return userMapper.fromUserEntityToUserDTO(userEntity);
        }else
            throw new UsernameNotFoundException("username already taken");
    }
}
