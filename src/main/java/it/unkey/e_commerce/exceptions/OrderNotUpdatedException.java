package it.unkey.e_commerce.exceptions;

public class OrderNotUpdatedException extends RuntimeException {
    public OrderNotUpdatedException(String message) {
        super(message);
    }
}
