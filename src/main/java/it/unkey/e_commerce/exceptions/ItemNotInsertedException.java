package it.unkey.e_commerce.exceptions;

public class ItemNotInsertedException extends RuntimeException  {
    public ItemNotInsertedException(String message) {
        super(message);
    }
}
