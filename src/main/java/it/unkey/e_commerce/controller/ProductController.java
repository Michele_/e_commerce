package it.unkey.e_commerce.controller;

import it.unkey.e_commerce.DTO.ProductDTO;
import it.unkey.e_commerce.DTO.response.ProductResponseAPI;
import it.unkey.e_commerce.enums.PatchUpdate;
import it.unkey.e_commerce.exceptions.IdNotFoundException;
import it.unkey.e_commerce.exceptions.ItemNotFoundException;
import it.unkey.e_commerce.exceptions.ItemNotInsertedException;
import it.unkey.e_commerce.interfaces.ProductService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Set;

@RestController
@RequestMapping("/product")
@CrossOrigin(origins = "*", allowedHeaders = "*")
@Slf4j
public class ProductController {

    private ProductService<ProductDTO> productService;

    @Autowired
    public ProductController(ProductService<ProductDTO> productService) {
        this.productService = productService;
    }

    @GetMapping("/id/{id}")
    public ResponseEntity<ProductDTO> getById(@PathVariable int id) {

        try {

            log.info("searching for product of id {}", id);
            return ResponseEntity.ok(productService.getById(id));

        } catch (IdNotFoundException e) {

            log.warn(e.getMessage());
            return ResponseEntity.notFound().build();
        }
    }

    @GetMapping
    public ResponseEntity<Set<ProductDTO>> getAll(){
        return ResponseEntity.ok(productService.getAll());

    }

    @GetMapping("/name/{name}")
    public ResponseEntity<ProductDTO> getByName(@PathVariable String name) {
        return ResponseEntity.ok(productService.getByName(name));
    }

    @PostMapping("/insert")
    public ResponseEntity<ProductResponseAPI> saveNewProduct(@RequestBody ProductDTO productDTO) {

        try {
            log.info("try to save a new product");
            ProductDTO product = productService.insert(productDTO);
            return ResponseEntity.created(new URI("/product/" + productDTO.getId())).body(new ProductResponseAPI(product));

        } catch (ItemNotInsertedException | URISyntaxException e) {

            log.warn(e.getMessage());
            return ResponseEntity.badRequest().body(new ProductResponseAPI(e.getMessage()));

        }
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProductResponseAPI> updateProductWithPut (@PathVariable int id, @RequestBody ProductDTO productDTO) {

        productDTO.setId(id);
        return tryToUpdateWithPutOrPatch(productDTO, PatchUpdate.PUT);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<ProductResponseAPI> updateProductWithPatch (@PathVariable int id, @RequestBody ProductDTO productDTO) {

        productDTO.setId(id);
        return tryToUpdateWithPutOrPatch(productDTO, PatchUpdate.PATCH);
    }

    private ResponseEntity<ProductResponseAPI> tryToUpdateWithPutOrPatch (ProductDTO productDTO, PatchUpdate updateType) {

        try {
            return updateWithPutOrPatch(productDTO, updateType);
        } catch (ItemNotFoundException e) {
            log.warn(e.getMessage());
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(new ProductResponseAPI(e.getMessage()));
        }

    }

    private ResponseEntity<ProductResponseAPI> updateWithPutOrPatch (ProductDTO productDTO, PatchUpdate updateType) {

        ProductDTO updatedProduct = switchPutOrUpdate(productDTO, updateType);
        return ResponseEntity.ok(new ProductResponseAPI(updatedProduct));
    }

    private ProductDTO switchPutOrUpdate (ProductDTO productDTO, PatchUpdate updateType) {

        switch (updateType) {
            case PUT:
                return productService.update(productDTO);
            case PATCH:
                return productService.patchUpdate(productDTO);
            default:
                return null;
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteProduct (@PathVariable int id) {

        try {

            productService.delete(id);
            return ResponseEntity.noContent().build();

        } catch (IdNotFoundException e) {

            log.warn(e.getMessage());
            return ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());
        }

    }

}
