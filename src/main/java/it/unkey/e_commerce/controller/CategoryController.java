package it.unkey.e_commerce.controller;

import it.unkey.e_commerce.DTO.CategoryDTO;
import it.unkey.e_commerce.interfaces.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

@RestController
@RequestMapping("/category")
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class CategoryController {

    private CategoryService<CategoryDTO> categoryService;

    @Autowired
    public CategoryController(@Qualifier("categoryService")CategoryService<CategoryDTO> categoryService) {
        this.categoryService = categoryService;
    }

    @GetMapping
    public ResponseEntity<Set<CategoryDTO>> getAll(){

        return ResponseEntity.ok(categoryService.getAll());

    }

    @GetMapping("/id/{id}")
    public ResponseEntity<CategoryDTO> getById(@PathVariable int id) {

        return ResponseEntity.ok(categoryService.getById(id));
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<CategoryDTO> getByName(@PathVariable String name) {

        return ResponseEntity.ok(categoryService.getByName(name));
    }

}
