package it.unkey.e_commerce.controller;

import it.unkey.e_commerce.DTO.OrderDTO;
import it.unkey.e_commerce.DTO.response.OrderResponseAPI;
import it.unkey.e_commerce.enums.PatchUpdate;
import it.unkey.e_commerce.exceptions.IdNotFoundException;
import it.unkey.e_commerce.exceptions.ItemNotFoundException;
import it.unkey.e_commerce.exceptions.OrderNotFoundException;
import it.unkey.e_commerce.exceptions.OrderNotUpdatedException;
import it.unkey.e_commerce.interfaces.OrderService;
import it.unkey.e_commerce.mappers.OrderMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Set;

@RestController
@RequestMapping("/orders")
@Slf4j
public class OrderController {

    private OrderMapper orderMapper;
    private OrderService<OrderDTO> orderService;

    @Autowired
    public OrderController(OrderService<OrderDTO> orderService, OrderMapper orderMapper) {
        this.orderService = orderService;
        this.orderMapper = orderMapper;
    }

    @GetMapping("{id}")
    public ResponseEntity<OrderDTO> getOrderById(@PathVariable int id) {

        try {

            log.info("searching for order of id {}", id);
            OrderDTO order = orderService.getById(id);
            return ResponseEntity.ok(order);

        } catch(RuntimeException e) {

            log.warn(e.getMessage());
            return ResponseEntity.notFound().build();
        }

    }

    @GetMapping
    public ResponseEntity<Set<OrderDTO>> getAllOrders() {

        Set<OrderDTO> orders = orderService.getAll();
        return ResponseEntity.ok(orders);
    }

    @PostMapping("/insert")
    public ResponseEntity<OrderResponseAPI> saveNewOrder(@RequestBody OrderDTO orderDTO) {

        try {

            log.info("trying to insert a new order...");
            OrderDTO newOrder = orderService.insert(orderDTO);
            return ResponseEntity
                    .created(new URI("/orders/" + newOrder.getId()))
                    .body(new OrderResponseAPI(newOrder));

        } catch(RuntimeException | URISyntaxException e) {
            log.warn(e.getMessage());
            return ResponseEntity.badRequest().body(new OrderResponseAPI(e.getMessage()));
        }
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<String> deleteOrder(@PathVariable int id) {

        try {
            orderService.delete(id);
            return ResponseEntity.noContent().build();
        } catch (IdNotFoundException e) {
            log.warn(e.getMessage());
            return  ResponseEntity.status(HttpStatus.NOT_FOUND).body(e.getMessage());

        }
    }



    @PatchMapping("/{id}")
    public ResponseEntity<OrderResponseAPI> updateOrderWithPatch (@PathVariable int id, @RequestBody OrderDTO orderDTO) {

        orderDTO.setId(id);
        return tryToUpdateWithPutOrPatch(orderDTO, PatchUpdate.PATCH);
    }

    private ResponseEntity<OrderResponseAPI> tryToUpdateWithPutOrPatch (OrderDTO orderDTO, PatchUpdate updateType) {

        try {
            return updateWithPutOrPatch(orderDTO, updateType);
        } catch (OrderNotFoundException e) {
            log.warn(e.getMessage());
            return ResponseEntity
                    .status(HttpStatus.NOT_FOUND)
                    .body(new OrderResponseAPI(e.getMessage()));
        }

    }

    private ResponseEntity<OrderResponseAPI> updateWithPutOrPatch (OrderDTO orderDTO, PatchUpdate updateType) {

        OrderDTO updatedOrder = switchPutOrUpdate(orderDTO, updateType);
        return ResponseEntity.ok(new OrderResponseAPI(updatedOrder));
    }

    private OrderDTO switchPutOrUpdate (OrderDTO orderDTO, PatchUpdate updateType) {

        switch (updateType) {
            case PUT:
                return orderService.update(orderDTO);
            case PATCH:
                return orderService.patchUpdate(orderDTO);
            default:
            throw new OrderNotUpdatedException("could not update this order");
        }
    }

}
