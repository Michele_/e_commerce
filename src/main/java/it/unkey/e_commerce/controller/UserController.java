package it.unkey.e_commerce.controller;

import it.unkey.e_commerce.DTO.LoginDTO;
import it.unkey.e_commerce.DTO.RegisterDTO;
import it.unkey.e_commerce.DTO.UserDTO;
import it.unkey.e_commerce.DTO.response.UserResponseAPI;
import it.unkey.e_commerce.entity.UserEntity;
import it.unkey.e_commerce.exceptions.UserNotRegisteredException;
import it.unkey.e_commerce.interfaces.UserService;
import it.unkey.e_commerce.repositories.ApplicationUserRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.bind.annotation.*;

import java.net.URI;
import java.net.URISyntaxException;

@RestController
@RequestMapping("/users")
@Slf4j
@CrossOrigin(origins = "*", allowedHeaders = "*")
public class UserController {

    private UserService<UserDTO, LoginDTO, RegisterDTO> userService;

    @Autowired
    public UserController(@Qualifier("userService") UserService<UserDTO, LoginDTO, RegisterDTO> userService) {
        this.userService = userService;
    }

    @PostMapping("/register")
    public ResponseEntity<UserResponseAPI> registerUser(@RequestBody RegisterDTO registerDTO) {

        try {

            UserDTO userDTO = userService.registerUser(registerDTO);
            return ResponseEntity.created(new URI("/users/" + userDTO.getId())).body(new UserResponseAPI(userDTO));

        } catch (UserNotRegisteredException | URISyntaxException e) {

            log.warn(e.getMessage());
            return ResponseEntity.badRequest().body(new UserResponseAPI(e.getMessage()));

        }
    }
}
