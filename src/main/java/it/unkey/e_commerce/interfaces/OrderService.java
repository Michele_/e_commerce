package it.unkey.e_commerce.interfaces;

public interface OrderService<T> extends MyCRUD<T> {

    T insert(T t);

    T patchUpdate(T t);

    T deleteProduct(T t, int id);

}
