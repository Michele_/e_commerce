package it.unkey.e_commerce.interfaces;

import java.util.Set;

public interface MyCRUD<T>{

    T getById(int id);

    Set<T> getAll();

    T update(T t);

    void delete(int id);

}
