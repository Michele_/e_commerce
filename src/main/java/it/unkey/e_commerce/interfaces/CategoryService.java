package it.unkey.e_commerce.interfaces;

import java.util.Set;

public interface CategoryService<T> {

    Set<T> getAll();

    T getById(int id);

    T getByName(String name);


}
