package it.unkey.e_commerce.interfaces;

public interface ProductService<T> extends MyCRUD<T> {

    T insert(T t);

    T patchUpdate(T t);

    T getByName (String name);
}
