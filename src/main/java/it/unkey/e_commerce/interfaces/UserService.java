package it.unkey.e_commerce.interfaces;

public interface UserService<U, L, R> extends MyCRUD<U>  {

    U getByUsernameAndPassword(L l);

    U registerUser(R r);

}
