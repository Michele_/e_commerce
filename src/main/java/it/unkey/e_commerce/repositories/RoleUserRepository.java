package it.unkey.e_commerce.repositories;

import it.unkey.e_commerce.entity.RoleUserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface RoleUserRepository extends JpaRepository<RoleUserEntity, Integer> {
}
