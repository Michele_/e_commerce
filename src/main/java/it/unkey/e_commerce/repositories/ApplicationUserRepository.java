package it.unkey.e_commerce.repositories;

import it.unkey.e_commerce.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicationUserRepository extends JpaRepository<UserEntity, Integer> {

    UserEntity findByName(String name);
    UserEntity findByNameAndPassword(String name, String password);
}
